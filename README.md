# Fork Mainnet

## 1. Install Packages
```
$ npm i
```

## 2. Set Environment

Create .env as below
```
FORK_MAINNET=true
FUND_FORK_MAINNET=true
RPC_NODE=https://mainnet.infura.io/v3/${Your Infura Key}
```

## 3. Start
```
$ npm run start
```
